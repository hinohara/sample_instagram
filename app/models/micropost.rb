class Micropost < ApplicationRecord
  belongs_to :user
  validates :picture, {presence: true}
  default_scope -> {order(created_at: :desc)}
  mount_uploader :picture, PictureUploader
  
  def self.search(search)
    if search
      Micropost.where(['content LIKE ?', "%#{search}%"])
    else
      Micropost.all
    end
  end
end
