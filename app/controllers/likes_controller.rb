class LikesController < ApplicationController
  def create
    @like = Like.new(
      user_id: current_user.id,
      post_id: params[:micropost_id]
      )
    @like.save
      redirect_to(root_path)
  end
  
  def destroy
    @likes = Like.find_by(
      user_id: @current_user.id,
      post_id: params[:micropost_id]
      )
     redirect_to("/users/index")
  end
  
end