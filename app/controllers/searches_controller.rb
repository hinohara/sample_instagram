class SearchesController < ApplicationController
  def index
    @searches = Search.search(params[:search])
  end
end