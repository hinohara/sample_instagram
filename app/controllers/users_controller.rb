class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]
  
  def facebook
    callback_for(:facebook)
  end
  
  def callback_for(provider)
    @user = User.from_omniauth(request.env["omniauth.auth"])
    if @user.persisted?
      sign_in_and_redirect @user, event: :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, kind: "#{provider}".capitalize) if is_navigational_format?
    else
      session["devise.#{provider}_data"] = request.env["omniauth.auth"].except("extra")
      redirect_to new_user_registration_url
    end
  end

  def failure
    redirect_to root_path
  end
  
  def index
     @users = User.all
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      redirect_to @user
    else
      render 'edit'
    end
  end

  def show
    @user = User.find(params[:id])
    @micropost = Micropost.find_by(id: params[:id])
    @microposts = @user.microposts
  end
  
  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.all
    render 'show_follow'
  end
  
  def followers
    @title = "Followed"
    @user = User.find(params[:id])
    @users = @user.followers.all
    render 'show_follow'
  end
  
  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end
  
  def user_params
    params.require(:user).permit(:username, :fullname, :website,
                                  :profile, :mailadress, :callnumber, :sex)
  end
  
  
end
