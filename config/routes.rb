Rails.application.routes.draw do
  
  get '/contact', to: 'static_pages#contact'
  get 'users/index'
  get 'users/show'
  get '/post', to: 'microposts#post'
  post "likes/:micropost_id/create" => "likes#create"
  post "likes/:micropost_id/destroy" => "likes#destroy"
  root 'static_pages#home'
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks'}
  resources :users, only: [:index, :show, :edit]
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :microposts
  resources :relationships, only:[:create, :destroy]
  resources :searches
end
