class AddReplyToToMicroposts < ActiveRecord::Migration[5.2]
  def change
    add_column :microposts, :reply_to, :integer
  end
end
