class RemoveMyselfFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :myself, :string
  end
end
