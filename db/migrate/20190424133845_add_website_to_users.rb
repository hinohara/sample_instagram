class AddWebsiteToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :website, :string
    add_column :users, :myself, :string
    add_column :users, :mailadress, :string
    add_column :users, :callnumber, :string
    add_column :users, :sex, :string
  end
end
